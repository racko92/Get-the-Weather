# GetTheWeather

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.0.

## App information

App displays current weather conditions.  
To select locations, type its name in search bar. App will add card that display current weather for that locations.  

Multiple city selections is working. To select multiple locations, enter location name and separate it from next one with `, ` (comma and space)


Location removal repmoves both card and bouble beside search bar.
Locations can be removed in two ways:
- Clicking `x` button left of search bar
- Clicking on card itself  


Project uses OpenWeatherMap( http://openweathermap.org/ ) free API for displaying current weather information.

## Installation guide: 

- Node.js [install guide](https://nodejs.org/en/download/package-manager/)
- Angular 5 [install guide](https://angular.io/guide/quickstart)
- For clone of project, run ```git clone https://gitlab.com/racko92/Get-the-Weather.git``` for `HTTPS`, or ```git clone git@gitlab.com:racko92/Get-the-Weather.git``` if you prefer `SSH`
- Run ```npm install``` when enter downloaded directory to install dependencies specified in `package.json`
- To run local development server, run `ng serve --open` to open page in default browser, or `ng serve` and then go to `http://localhost:4200/`.  The app will automatically reload if you change any of the source files.


## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
