import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http/src/response';

@Injectable()
export class ApiService {
    private weatherData: any;
    private weatherLocations: any;
    private locationSearchError: boolean = false;

    constructor(
        private http: HttpClient,
    ) { }
    getCityDataByLocation(weatherLocation){
        return new Observable((o: Observer<any>) => {
            this.http.get('http://api.openweathermap.org/data/2.5/weather?q='+ weatherLocation +'&APPID=637e7c3bcc383abddb016991b3770f81&units=metric')
            .subscribe(weatherData => {                
                this.weatherData = JSON.stringify({weatherData});
                o.next(weatherData);
                return o.complete();
            }, (err: HttpErrorResponse) => {
                console.log(err.error);
                alert(`Entered location '${weatherLocation}' not found!`);
            })
        })
    }
}