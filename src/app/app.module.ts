import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { CitySearchComponent } from './components/city-search/city-search.component';
import { SharedModule } from './shared/shared.module';
import { WeatherCardComponent } from './components/weather-card/weather-card.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CapitalizeFirstPipe } from './shared/pipes/capitalizefirst.pipe';

@NgModule({
  declarations: [
    AppComponent,
    CitySearchComponent,
    WeatherCardComponent,
    CapitalizeFirstPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    SharedModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
