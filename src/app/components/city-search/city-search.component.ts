import { Component, OnInit, Injector } from '@angular/core';
import { ApiService } from './../../shared/services/api.service';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { FormGroup, FormControl, FormBuilder, ReactiveFormsModule, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'app-city-search',
    templateUrl: './city-search.component.html',
    styleUrls: ['./city-search.component.css']
})

export class CitySearchComponent implements OnInit {

    private weatherData: any = [];
    private weatherForm: FormGroup;
    private weatherLocations: any = [];
    private error: any;

    constructor(
        private injector: Injector,
        private apiService: ApiService,
        private formBuilder: FormBuilder,
    ) {
        this.weatherForm = formBuilder.group({
            'location': [
                '',
                [
                    Validators.minLength(2),
                    Validators.required,
                    this.duplicateCitySearch.bind(this)
                ],
            ]
        });
    }

    ngOnInit() {
        this.weatherData;        
    }

    searchLocation() {
        let locations = this.weatherForm.value.location.split(", ");
        let formattedLocations: any = [];
        locations.forEach(
            element => {
            formattedLocations.push(element.toLowerCase())
        });

        formattedLocations.forEach(
            element => {
                this.apiService = this.injector.get(ApiService);
                this.apiService.getCityDataByLocation(element).subscribe(
                    data => {
                        this.weatherData.push(Array.of(data));
                        this.weatherLocations.push(element.toLowerCase());
                    }
                )
            }
        );
        this.weatherForm.reset();
    }
    
    removeCity(weatherData){
        let index = this.weatherData.indexOf(weatherData);
        let cityName = weatherData[0].name.toLowerCase();
        this.weatherLocations.splice(this.weatherLocations.indexOf(cityName), 1);
        return this.weatherData.splice(index,1);
    }

    duplicateCitySearch(control: FormControl): {[s: string]: boolean} {
        if (typeof(this.weatherLocations) === 'undefined' || control.value === null) {
            return null;
        }
        
        let locations = control.value.split(", ");

        for (let location of locations) {
            if (this.weatherLocations.indexOf(location.toLowerCase()) != -1) {
                return {duplicate: true};
            }
            return null;
        }
        return null;     
    }
}
