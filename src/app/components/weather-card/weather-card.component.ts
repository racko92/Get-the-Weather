import { Component, Input, Output, EventEmitter, SimpleChange } from '@angular/core';
import { CitySearchComponent } from './../city-search/city-search.component';
import { CapitalizeFirstPipe } from './../../shared/pipes/capitalizefirst.pipe';

@Component({
  selector: '[weatherCard]',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.css']
})

export class WeatherCardComponent {

  @Input()
  set weatherCard(weatherCardData){
    this.weatherCardData = weatherCardData;
  }
  constructor(private citySearch: CitySearchComponent) { }

  private weatherCardData;
  utc = new Date();

  removeCard(weatherCardData) {
    this.citySearch.removeCity(weatherCardData);
  }
}
